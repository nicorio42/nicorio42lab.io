---
title: Ingénieur développement web
company: CGI
location: Grenoble
duration: d'octobre 2021 à actuellement
---

Développement et maintenance d'une application métier de gestion d'installations énergétiques : télégestion d'automates industriels, et télécollecte/traitement des données.

Rôle de développeur full stack (Angular, Spring Boot, Oracle), dans une équipe AGILE de 10 personnes.
