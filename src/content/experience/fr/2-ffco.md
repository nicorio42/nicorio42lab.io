---
title: Consultant indépendant
company: FFCO
location: Freelance
duration: de janvier a décembre 2022
---

Conception et réalisation d'une application web pour l'analyse des entraînements des athlètes de l'équipe de France de Course d'Orientation. Travail en tant que prestataire indépendant pour la Fédération Française.

Utilisation de Python Django, puis de Svelte JS et Firebase.
