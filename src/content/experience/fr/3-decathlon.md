---
title: Ingénieur R&D
company: Décathlon
location: Lille
duration: de novembre 2019 à juin 2020
---

Conception et amélioration de la gamme des boussoles de course d'orientation.

Création d'un modèle numérique de la dynamique de l’aiguille à l’aide du langage Python.

Conception du [modèle haut de gamme R900](https://www.decathlon.fr/p/boussole-pouce-gauche-pour-course-d-orientation-racer-900-noir/_/R-p-313026?mc=8576047).
