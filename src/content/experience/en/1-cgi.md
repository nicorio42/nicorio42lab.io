---
title: Web development engineer
company: CGI
location: Grenoble
duration: october 2021 to now
---

Development and maintainance of a business-scale application.

Développement et maintenance d'une application métier de télégestion d'automates industriels, et de télécollecte de données dans le domaine de la gestion d'installations energétiques.

Rôle de développeur full stack (Angular, Spring Boot, Oracle), dans une équipe AGILE de 10 personnes.
