declare module 'astro:content' {
	export { z } from 'astro/zod';
	export type CollectionEntry<C extends keyof typeof entryMap> =
		typeof entryMap[C][keyof typeof entryMap[C]] & Render;

	type BaseCollectionConfig<S extends import('astro/zod').ZodRawShape> = {
		schema?: S;
		slug?: (entry: {
			id: CollectionEntry<keyof typeof entryMap>['id'];
			defaultSlug: string;
			collection: string;
			body: string;
			data: import('astro/zod').infer<import('astro/zod').ZodObject<S>>;
		}) => string | Promise<string>;
	};
	export function defineCollection<S extends import('astro/zod').ZodRawShape>(
		input: BaseCollectionConfig<S>
	): BaseCollectionConfig<S>;

	export function getEntry<C extends keyof typeof entryMap, E extends keyof typeof entryMap[C]>(
		collection: C,
		entryKey: E
	): Promise<typeof entryMap[C][E] & Render>;
	export function getCollection<
		C extends keyof typeof entryMap,
		E extends keyof typeof entryMap[C]
	>(
		collection: C,
		filter?: (data: typeof entryMap[C][E]) => boolean
	): Promise<(typeof entryMap[C][E] & Render)[]>;

	type InferEntrySchema<C extends keyof typeof entryMap> = import('astro/zod').infer<
		import('astro/zod').ZodObject<Required<ContentConfig['collections'][C]>['schema']>
	>;

	type Render = {
		render(): Promise<{
			Content: import('astro').MarkdownInstance<{}>['Content'];
			headings: import('astro').MarkdownHeading[];
			injectedFrontmatter: Record<string, any>;
		}>;
	};

	const entryMap: {
		"biography": {
"en.md": {
  id: "en.md",
  slug: "en",
  body: string,
  collection: "biography",
  data: InferEntrySchema<"biography">
},
"fr.md": {
  id: "fr.md",
  slug: "fr",
  body: string,
  collection: "biography",
  data: InferEntrySchema<"biography">
},
},
"education": {
"en/engineer-diploma.md": {
  id: "en/engineer-diploma.md",
  slug: "en/engineer-diploma",
  body: string,
  collection: "education",
  data: InferEntrySchema<"education">
},
"en/erasmus-trondheim.md": {
  id: "en/erasmus-trondheim.md",
  slug: "en/erasmus-trondheim",
  body: string,
  collection: "education",
  data: InferEntrySchema<"education">
},
"fr/engineer-diploma.md": {
  id: "fr/engineer-diploma.md",
  slug: "fr/engineer-diploma",
  body: string,
  collection: "education",
  data: InferEntrySchema<"education">
},
"fr/erasmus-trondheim.md": {
  id: "fr/erasmus-trondheim.md",
  slug: "fr/erasmus-trondheim",
  body: string,
  collection: "education",
  data: InferEntrySchema<"education">
},
},
"experience": {
"en/1-cgi.md": {
  id: "en/1-cgi.md",
  slug: "en/1-cgi",
  body: string,
  collection: "experience",
  data: InferEntrySchema<"experience">
},
"fr/1-cgi.md": {
  id: "fr/1-cgi.md",
  slug: "fr/1-cgi",
  body: string,
  collection: "experience",
  data: InferEntrySchema<"experience">
},
"fr/2-ffco.md": {
  id: "fr/2-ffco.md",
  slug: "fr/2-ffco",
  body: string,
  collection: "experience",
  data: InferEntrySchema<"experience">
},
"fr/3-decathlon.md": {
  id: "fr/3-decathlon.md",
  slug: "fr/3-decathlon",
  body: string,
  collection: "experience",
  data: InferEntrySchema<"experience">
},
},
"side-projects": {
"en/mapant.md": {
  id: "en/mapant.md",
  slug: "en/mapant",
  body: string,
  collection: "side-projects",
  data: InferEntrySchema<"side-projects">
},
"fr/1-mapant.md": {
  id: "fr/1-mapant.md",
  slug: "fr/1-mapant",
  body: string,
  collection: "side-projects",
  data: InferEntrySchema<"side-projects">
},
"fr/2-sport.md": {
  id: "fr/2-sport.md",
  slug: "fr/2-sport",
  body: string,
  collection: "side-projects",
  data: InferEntrySchema<"side-projects">
},
},

	};

	type ContentConfig = typeof import("./config");
}
