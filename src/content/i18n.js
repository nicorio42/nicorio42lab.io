export const translations = {
  page: {
    title: {
      fr: "Nicolas Rio | Ingénieur développement web",
      en: "Nicolas Rio | Web development engineer",
    },
  },
  header: {
    profilePictureAlt: {
      fr: "Photo de profile de Nicolas Rio",
      en: "Profile picture of Nicolas Rio",
    },
    job: { fr: "Ingénieur développement web", en: "Web development engineer" },
    catchPhrase: {
      fr: "Disponible à partir de mai 2023",
      en: "Available from may 2023",
    },
  },
  experience: { title: { fr: "Expérience", en: "Experience" } },
  education: { title: { fr: "Formation", en: "Education" } },
  sideProjects: { title: { fr: "Projets personnels", en: "Side projects" } },
  skills: { title: { fr: "Compétences", en: "Skills" } },
  languages: { title: { fr: "Langues", en: "Languages" } },
  biography: { title: { fr: "Mon parcours", en: "Background" } },
};
