import { z, defineCollection } from "astro:content";

const experience = defineCollection({
  schema: {
    title: z.string(),
    company: z.string(),
    location: z.string(),
    duration: z.string().optional(),
    from: z.string().optional(),
    to: z.string().optional(),
  },
});

const education = defineCollection({
  schema: {
    title: z.string(),
    company: z.string(),
    location: z.string(),
    duration: z.string().optional(),
    from: z.string().optional(),
    to: z.string().optional(),
  },
});

const sideProjects = defineCollection({
  schema: {
    title: z.string(),
    company: z.string().optional(),
    location: z.string().optional(),
    duration: z.string().optional(),
    from: z.string().optional(),
    to: z.string().optional(),
  },
});

const biography = defineCollection({
  schema: {},
});

export const collections = {
  experience,
  education,
  "side-projects": sideProjects,
  biography,
};
