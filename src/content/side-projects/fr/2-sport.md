---
title: Sportif de haut niveau
company: FFCO
duration: de 2010 à 2022
---

Membre de l'équipe de France de course d'orientation. Participation aux compétitions internationales avec comme meilleur résultat deux médailles de bronze au championnats du monde en 2018 et 2019 sur le format relais.
