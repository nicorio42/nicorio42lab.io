---
title: Mapant.fr
---

Génération automatique de cartes de course d'orientation à partir de données LiDAR.

Site internet : [mapant.fr](https://mapant.fr/)

Code source : [mapant-website](https://github.com/NicoRio42/mapant-website) et [mapant-scripts](https://github.com/NicoRio42/mapant-scripts)
