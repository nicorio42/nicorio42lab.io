import type { collections } from "./config";

export type CollectionsNames = keyof typeof collections;
