---
title: Modélisation des systèmes
company: NTNU
location: Trondheim (Norvège)
---

Semestre d'échange au National Institute of Technologies de Trondheim
(Norvège). Spécialisation en modélisation des systèmes pour l'ingénierie
en Python.
