---
title: Diplôme d'ingénieur
company: INSA Lyon
location: Lyon
---

Formation d'ingénieur au sein du département Génie Mécanique de l'INSA
Lyon. Spécialisation en conception et études.
