export const skills = [
  { name: "Python", score: 2 },
  { name: "Django", score: 3 },
  { name: "Javascript", score: 4 },
  { name: "Angular", score: 4 },
  { name: "Node js", score: 3 },
  { name: "Svelte js", score: 4 },
  { name: "React js", score: 1 },
  { name: "Vue js", score: 1 },
  { name: "Java", score: 2 },
  { name: "Spring Boot", score: 2 },
  { name: "HTML/CSS", score: 4 },
  { name: "Tailwind CSS", score: 2 },
  { name: "Git", score: 2 },
  { name: "Firebase", score: 2 },
  { name: "Leaflet", score: 2 },
  { name: "Openlayers", score: 2 },
  { name: "SQL", score: 2 },
];

export const softSkills = [
  { name: { fr: "Gestion de projet", en: "Project management" }, score: 3 },
  { name: { fr: "Méthodologie AGILE", en: "AGILE method" }, score: 3 },
  { name: { fr: "Conception produits", en: "Product design" }, score: 4 },
  {
    name: { fr: "Modélisation systèmes", en: "Systems modeling" },
    score: 4,
  },
];

export const languages = [
  { name: { fr: "Français", en: "French" }, score: 5 },
  { name: { fr: "Anglais", en: "English" }, score: 3 },
  { name: { fr: "Allemand", en: "German" }, score: 1 },
];
