import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";

export default defineConfig({
  integrations: [tailwind()],
  sitemap: true,
  site: "https://nicolas-rio.com/",
  outDir: "public",
  publicDir: "static",
  experimental: {
    contentCollections: true,
  },
});
